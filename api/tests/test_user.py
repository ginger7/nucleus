import json

import pkg_resources
import pytest

from tft.nucleus.api.public import api

from .conftest import assets, internal_client


def test_create_user(add_user):
    """
    Test the create user endpoint.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_user_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    assert content == expected_response


def test_create_user_bad_api_key(add_user):
    """
    Test the create user endpoint with a bad api_key.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_bad_api_key'])
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'


def test_get_user(add_user):
    """
    Test the get user endpoint.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.get('/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_user_response_example'].copy()
    expected_response['id'] = create_response_content['id']
    expected_response['created'] = create_response_content['created']
    expected_response['updated'] = create_response_content['updated']
    assert content == expected_response


def test_get_user_no_auth_bad_key(add_user):
    """
    Test the get user endpoint with no auth and with a bad api_key.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.get('/v0.1/users/{}'.format(create_response_content['id']))

    assert response.status_code == 422

    response = internal_client.get(
        '/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['bad_api_key'])
    )
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'


def test_get_users(delete_all_users, add_user):
    """
    Test the get users endpoint.
    """
    expected_responses = []

    # First of all, get expected response for already created user
    # and in meanwhile test get_users with only one user
    response = internal_client.get('/v0.1/users/?api_key={}'.format(assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 200

    expected_responses.append(content[0])

    for _ in range(3):
        # Create 3 user, one is already created by the fixture
        response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
        content = json.loads(response.content)

        assert response.status_code == 200

        expected_response = assets['create_user_response_example'].copy()
        expected_response['id'] = content['id']
        expected_response['created'] = content['created']
        expected_response['updated'] = content['updated']
        expected_responses.append(expected_response)
        assert content == expected_response

    response = internal_client.get('/v0.1/users?api_key={}'.format(assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 200

    assert sorted(content, key=lambda x: x['id']) == sorted(expected_responses, key=lambda x: x['id'])


def test_get_users_no_auth_bad_key(add_user):
    """
    Test the get users endpoint with no auth and with a bad api_key.
    """
    response = internal_client.get('/v0.1/users/')

    assert response.status_code == 422

    response = internal_client.get('/v0.1/users/?api_key={}'.format(assets['bad_api_key']))
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'


def test_update_user(add_user):
    """
    Test the update user endpoint.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.put(
        '/v0.1/users/{}'.format(create_response_content['id']), json=assets['update_user_example']
    )
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['update_user_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']
    assert content == expected_response


def test_update_user_bad_api_key(add_user):
    """
    Test the update user endpoint with a bad api_key.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.put(
        '/v0.1/users/{}'.format(create_response_content['id']), json=assets['update_user_bad_api_key']
    )
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'


def test_delete_user(add_user):
    """
    Test the delete user endpoint.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.delete(
        '/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )

    assert response.status_code == 200

    response = internal_client.get('/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['api_key']))
    content = json.loads(response.content)

    assert response.status_code == 404

    assert content['message'] == 'No such entity'


def test_delete_user_bad_api_key(add_user):
    """
    Test the delete user endpoint with a bad key.
    """
    response = internal_client.post('/v0.1/users', json=assets['create_user_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.delete(
        '/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['bad_api_key'])
    )
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'

    response = internal_client.get('/v0.1/users/{}?api_key={}'.format(create_response_content['id'], assets['api_key']))

    assert response.status_code == 200
