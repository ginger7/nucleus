import json

from .conftest import assets, public_client


def test_get_composes():
    """
    Test the create test request endpoint.
    """
    response = public_client.get('/v0.1/composes')
    content = json.loads(response.content)

    assert response.status_code == 200

    expected_response = assets['public_composes_response_example'].copy()
    assert content == expected_response


def test_get_ranch_composes():
    """
    Test the create test request endpoint.
    """
    response = public_client.get('/v0.1/composes/redhat')
    content = json.loads(response.content)

    assert response.status_code == 200

    expected_response = assets['redhat_composes_response_example'].copy()
    assert content == expected_response
