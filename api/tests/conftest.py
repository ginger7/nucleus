from uuid import uuid4

import pytest
from fastapi.testclient import TestClient
from ruamel.yaml import YAML

from tft.nucleus.api.core.database import Request, SessionLocal, User
from tft.nucleus.api.internal import api as internal_api
from tft.nucleus.api.public import api as public_api

public_client = TestClient(public_api)
internal_client = TestClient(internal_api)

# Path to assets relative to tox location
ASSERTS_PATH = "tests/assets.yml"


def get_assets():
    yaml = YAML(typ='safe')
    with open(ASSERTS_PATH, 'r') as assets_file:
        assets = yaml.load(assets_file)
    return assets


assets = get_assets()


# We need to have at least one user to use the API
@pytest.fixture
def add_user():
    session = SessionLocal()
    user_db = User(id=str(uuid4()), name='test-name', api_key='test-key', enabled=True)
    session.add(user_db)
    session.commit()
    session.refresh(user_db)
    session.close()


@pytest.fixture
def delete_all_requests():
    session = SessionLocal()
    session.query(Request).delete()
    session.commit()
    session.close()


@pytest.fixture
def delete_all_users(delete_all_requests):
    session = SessionLocal()
    session.query(User).delete()
    session.commit()
    session.close()
