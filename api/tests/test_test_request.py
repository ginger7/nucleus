import json
from time import sleep

import pkg_resources
import pytest

from tft.nucleus.api.public import api

from .conftest import assets, internal_client, public_client


def test_create_test_request(add_user):
    """
    Test the create test request endpoint.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    content = json.loads(response.content)

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_test_request_response_example'].copy()
    expected_response['id'] = content['id']
    expected_response['created'] = content['created']
    expected_response['updated'] = content['updated']

    assert content == expected_response


def test_create_test_request_bad_api_key(add_user):
    """
    Test the create test request endpoint with bad api key.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_bad_api_key'])
    content = json.loads(response.content)

    assert response.status_code == 401

    assert content['message'] == 'Not authorized to perform this action'


def test_create_test_request_too_much_test_types(add_user):
    """
    Test the create test request endpoint with too much tests types.
    """
    response = public_client.post("/v0.1/requests", json=assets['test_request_too_much_test_types'])
    content = json.loads(response.content)

    assert response.status_code == 422
    assert content['message'] == 'Test section has more than one type.'


def test_create_test_request_no_test(add_user):
    """
    Test the create test request endpoint with no tests.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_no_tests'])
    content = json.loads(response.content)

    assert response.status_code == 422
    assert content['message'] == 'Test section is empty or test type is wrong.'


def test_get_test_request_public(add_user):
    """
    Test the get test request endpoint.
    """
    response = public_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = public_client.get('/v0.1/requests/{}'.format(create_response_content['id']))

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_test_request_response_example'].copy()
    expected_response['id'] = create_response_content['id']
    expected_response['created'] = create_response_content['created']
    expected_response['updated'] = create_response_content['updated']
    assert create_response_content == expected_response


def test_get_test_request_internal(add_user):
    """
    Test the get test request endpoint.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    response = internal_client.get('/v0.1/requests/{}'.format(create_response_content['id']))

    assert response.status_code == 422

    response = internal_client.get(
        '/v0.1/requests/{}?api_key={}'.format(create_response_content['id'], assets['api_key'])
    )

    assert response.status_code == 200

    # Before asserting we need to change placeholders to actual data
    expected_response = assets['create_test_request_response_example'].copy()
    expected_response['id'] = create_response_content['id']
    expected_response['created'] = create_response_content['created']
    expected_response['updated'] = create_response_content['updated']
    assert create_response_content == expected_response


def test_get_test_request_not_exists(add_user):
    """
    Test the get test request endpoint with not existing test request.
    """
    response = public_client.get('/v0.1/requests/5e64028c-b17e-4fd8-b16e-5fffa881ec31')
    content = json.loads(response.content)

    assert response.status_code == 404

    assert content['message'] == 'No such entity'


def test_update_test_request_queued_run_time(add_user):
    """
    Test the update test request endpoint if it fills in queued_time.
    """
    response = internal_client.post('/v0.1/requests', json=assets['test_request_example'])
    create_response_content = json.loads(response.content)

    assert response.status_code == 200

    sleep(5)  # imitation of queue

    response = internal_client.put(
        '/v0.1/requests/{}'.format(create_response_content['id']), json=assets['test_request_running_update']
    )
    content = json.loads(response.content)

    assert response.status_code == 200
    assert content['state'] == 'running'
    assert content['queued_time'] is not None
    # Check if queued_time changed to something
    assert content['queued_time'] >= 5
    # Check if queued_time do not have problems with time zones
    assert content['queued_time'] < 10

    sleep(5)  # imitation of running

    response = internal_client.put(
        '/v0.1/requests/{}'.format(create_response_content['id']), json=assets['test_request_complete_update']
    )
    content = json.loads(response.content)

    assert response.status_code == 200
    assert content['state'] == 'complete'
    # Check if queued_time changed to something
    assert content['run_time'] >= 10
    # Check if queued_time do not have problems with time zones
    assert content['run_time'] < 15
