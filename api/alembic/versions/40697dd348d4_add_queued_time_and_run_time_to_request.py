"""add queued time and run time to request

Revision ID: 40697dd348d4
Revises: 1664c4c54ce4
Create Date: 2021-12-15 16:34:33.218436

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = '40697dd348d4'
down_revision = '1664c4c54ce4'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('requests', sa.Column('queued_time', sa.Interval()))
    op.add_column('requests', sa.Column('run_time', sa.Interval()))


def downgrade():
    op.drop_column('requests', 'run_time')
    op.drop_column('requests', 'queued_time')
