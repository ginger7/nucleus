# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API.
"""
from dynaconf import settings
from fastapi import FastAPI

from .core.errors import NucleusException, nucleus_exception_handler
from .v0_1.internal import internal_api_router_v0_1

api = FastAPI(
    title='Testing Farm Private API',
)
api.include_router(internal_api_router_v0_1, prefix=settings.VERSIONS.V0_1.PREFIX)

api.add_exception_handler(NucleusException, nucleus_exception_handler)
