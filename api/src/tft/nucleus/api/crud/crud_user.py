# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of users Create, Read, Update, and Delete database operations.
"""
from typing import List
from uuid import UUID, uuid4

from sqlalchemy.orm import Session

from ..core import errors
from ..core.database import User
from ..core.schemes import user


def create_user(session: Session, user_in: user.UserCreateUpdateIn) -> user.UserCreateGetUpdateOut:
    """
    Create a user in database and transform it to response schema.
    """
    auth_user = session.query(User).filter(User.api_key == user_in.api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError
    user_id = str(uuid4())
    user_db = User(id=user_id, name=user_in.name, api_key=user_in.user_api_key, enabled=user_in.enabled)
    session.add(user_db)
    session.commit()
    session.refresh(user_db)
    user_out = user.UserCreateGetUpdateOut(
        id=user_db.id,
        name=user_db.name,
        api_key=user_db.api_key,
        enabled=user_db.enabled,
        created=user_db.created,
        updated=user_db.updated,
    )
    return user_out


def get_user(session: Session, user_id: UUID, api_key: str) -> user.UserCreateGetUpdateOut:
    """
    Retrieve user from database and transform to response schema.
    """
    auth_user = session.query(User).filter(User.api_key == api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    user_db = session.query(User).filter(User.id == str(user_id)).first()
    if not user_db:
        raise errors.NoSuchEntityError()

    user_out = user.UserCreateGetUpdateOut(
        id=user_db.id,
        name=user_db.name,
        api_key=user_db.api_key,
        enabled=user_db.enabled,
        created=user_db.created,
        updated=user_db.updated,
    )
    return user_out


def get_users(session: Session, api_key: str) -> List[user.UserCreateGetUpdateOut]:
    """
    Retrieve users from database and transform to response schemes list.
    """
    auth_user = session.query(User).filter(User.api_key == api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    users_db = session.query(User).all()
    if not users_db:
        raise errors.NoSuchEntityError()

    users_out = []
    for user_db in users_db:
        users_out.append(
            user.UserCreateGetUpdateOut(
                id=user_db.id,
                name=user_db.name,
                api_key=user_db.api_key,
                enabled=user_db.enabled,
                created=user_db.created,
                updated=user_db.updated,
            )
        )
    return users_out


def update_user(session: Session, user_id: UUID, user_in: user.UserCreateUpdateIn) -> user.UserCreateGetUpdateOut:
    """
    Update user in database and return response schema.
    """
    auth_user = session.query(User).filter(User.api_key == user_in.api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    update_user_db = session.query(User).filter(User.id == str(user_id)).first()
    if not update_user_db:
        raise errors.NoSuchEntityError()

    # For enabled flag we can't just do `user_in.enabled or update_user_db.enabled` (False or True will return False)
    if user_in.enabled is not None:
        enabled = user_in.enabled
    else:
        enabled = update_user_db.enabled

    updated_user_db = (
        session.query(User)
        .filter(User.id == str(user_id))
        .update(
            {
                'name': user_in.name or update_user_db.name,
                'api_key': user_in.user_api_key or update_user_db.api_key,
                'enabled': enabled,
            }
        )
    )

    if not updated_user_db:
        raise errors.NoSuchEntityError()

    session.commit()

    user_db = session.query(User).filter(User.id == str(user_id)).first()
    assert user_db is not None

    user_out = user.UserCreateGetUpdateOut(
        id=user_db.id,
        name=user_db.name,
        api_key=user_db.api_key,
        enabled=user_db.enabled,
        created=user_db.created,
        updated=user_db.updated,
    )
    return user_out


def delete_user(session: Session, user_id: UUID, api_key: str) -> None:
    """
    Delete the user from database.
    """
    auth_user = session.query(User).filter(User.api_key == api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    user_db = session.query(User).filter(User.id == str(user_id)).first()
    if not user_db:
        raise errors.NoSuchEntityError()

    session.delete(user_db)  # type: ignore
    session.commit()
