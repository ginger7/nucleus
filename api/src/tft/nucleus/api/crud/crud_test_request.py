# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of test requests Create, Read, Update, and Delete database operations.
"""
from datetime import datetime
from uuid import UUID, uuid4

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from ..core import errors
from ..core.database import Request, RequestStateType, User
from ..core.schemes import test_request


def get_test_request(session: Session, request_id: UUID) -> test_request.RequestGetUpdateOut:
    """
    Retrieve request from database and transform to response schema.
    """
    request = session.query(Request).filter(Request.id == request_id).first()
    if not request:
        raise errors.NoSuchEntityError()
    request_out = test_request.RequestGetUpdateOut(
        id=request.id,
        user_id=request.user_id,
        test=request.test,
        # state=request.state,  # TODO: uncomment once database migrated
        state=request.state.name.lower(),
        environments_requested=request.environments_requested,
        notes=request.notes,
        result=request.result,
        run=request.run,
        settings=request.settings,
        queued_time=request.queued_time,
        run_time=request.run_time,
        created=request.created,
        updated=request.updated,
    )
    return request_out


def get_test_request_authenticated(
    session: Session, request_id: UUID, api_key: str
) -> test_request.RequestGetUpdateOut:
    """
    Authenticate and retrieve request from database and transform to response schema.
    """

    auth_user = session.query(User).filter(User.api_key == api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    return get_test_request(session, request_id)


def create_test_request(
    session: Session, test_request_create_in: test_request.RequestCreateIn
) -> test_request.RequestCreateOut:
    """
    Create test request in database and transform it to response schema.
    """
    auth_user = session.query(User).filter(User.api_key == test_request_create_in.api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError
    request_id = str(uuid4())
    db_test_request = Request(
        id=request_id,
        user_id=auth_user.id,
        test=jsonable_encoder(test_request_create_in.test),
        environments_requested=jsonable_encoder(test_request_create_in.environments),
        notification=jsonable_encoder(test_request_create_in.notification),
        settings=jsonable_encoder(test_request_create_in.settings),
    )
    session.add(db_test_request)
    session.commit()
    session.refresh(db_test_request)
    request_out = test_request.RequestCreateOut(
        id=request_id,
        test=test_request_create_in.test,
        notification=test_request_create_in.notification,
        environments=test_request_create_in.environments,
        settings=test_request_create_in.settings,
        created=db_test_request.created,
        updated=db_test_request.updated,
        state=db_test_request.state.name.lower(),  # TODO: remove once database migrated
    )
    return request_out


def update_test_request(
    session: Session, request_id: UUID, test_request_in: test_request.RequestUpdateIn
) -> test_request.RequestGetUpdateOut:
    """
    Update test request in database and return response schema.
    """
    auth_user = session.query(User).filter(User.api_key == test_request_in.api_key).first()
    if not auth_user:
        raise errors.NotAuthorizedError

    update_test_request_db = session.query(Request).filter(Request.id == str(request_id)).first()
    if not update_test_request_db:
        raise errors.NoSuchEntityError()

    queued_time = None
    if test_request_in.state == RequestStateType.RUNNING:
        queued_time = datetime.utcnow() - update_test_request_db.created

    run_time = None
    if test_request_in.state in [RequestStateType.COMPLETE, RequestStateType.ERROR]:
        run_time = datetime.utcnow() - update_test_request_db.created

    updated_test_request_db = (
        session.query(Request)
        .filter(Request.id == str(request_id))
        .update(
            {
                'state': test_request_in.state or update_test_request_db.state,
                'notes': test_request_in.notes or update_test_request_db.notes,
                'environments_requested': test_request_in.environments_requested
                or update_test_request_db.environments_requested,
                'result': test_request_in.result or update_test_request_db.result,
                'run': test_request_in.run or update_test_request_db.run,
                'notification': test_request_in.notification or update_test_request_db.notification,
                'queued_time': queued_time or update_test_request_db.queued_time,
                'run_time': run_time or update_test_request_db.run_time,
            }
        )
    )

    if not updated_test_request_db:
        raise errors.NoSuchEntityError()

    session.commit()

    test_request_db = session.query(Request).filter(Request.id == str(request_id)).first()
    assert test_request_db is not None

    test_request_out = test_request.RequestGetUpdateOut(
        id=test_request_db.id,
        user_id=test_request_db.user_id,
        test=test_request_db.test,
        # state=test_request_db.state,  # TODO: uncomment once database migrated
        state=test_request_db.state.name.lower(),
        environments_requested=test_request_db.environments_requested,
        notes=test_request_db.notes,
        result=test_request_db.result,
        run=test_request_db.run,
        settings=test_request_db.settings,
        queued_time=test_request_db.queued_time,
        run_time=test_request_db.run_time,
        created=test_request_db.created,
        updated=test_request_db.updated,
    )
    return test_request_out
