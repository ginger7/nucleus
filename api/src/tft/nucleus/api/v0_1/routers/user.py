# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides implementation of users router
"""
from typing import List
from uuid import UUID

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from ...core.database import get_db
from ...core.schemes import user
from ...crud import crud_user

router = APIRouter()


@router.post('', response_model=user.UserCreateGetUpdateOut)
def create_user(user_in: user.UserCreateUpdateIn, session: Session = Depends(get_db)) -> user.UserCreateGetUpdateOut:
    """
    Create new user handler
    """
    return crud_user.create_user(session, user_in)


@router.put('/{user_id}', response_model=user.UserCreateGetUpdateOut)
def update_user(
    user_id: str, user_in: user.UserCreateUpdateIn, session: Session = Depends(get_db)
) -> user.UserCreateGetUpdateOut:
    """
    Update new user handler
    """
    return crud_user.update_user(session, UUID(user_id), user_in)


@router.get('/{user_id}', response_model=user.UserCreateGetUpdateOut)
def get_user(user_id: str, api_key: str, session: Session = Depends(get_db)) -> user.UserCreateGetUpdateOut:
    """
    Get user handler
    """
    return crud_user.get_user(session, UUID(user_id), api_key)


@router.get('', response_model=List[user.UserCreateGetUpdateOut])
def get_users(api_key: str, session: Session = Depends(get_db)) -> List[user.UserCreateGetUpdateOut]:
    """
    Get users handler
    """
    return crud_user.get_users(session, api_key)


@router.delete('/{user_id}')
def delete_user(user_id: str, api_key: str, session: Session = Depends(get_db)) -> None:
    """
    Delete user handler
    """
    return crud_user.delete_user(session, UUID(user_id), api_key)
