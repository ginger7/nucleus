# Copyright Contributors to the Testing Farm project.
# SPDX-License-Identifier: Apache-2.0
"""
The module provides public implementation of testing farm API v0.1.
"""
from typing import Dict

from fastapi import APIRouter

from ..about import about_get
from .routers import compose, test_request

public_api_router_v0_1 = APIRouter()


@public_api_router_v0_1.get('/about', summary='About Testing Farm')
def get_about() -> Dict[str, str]:
    """
    The function returns metadata about nucleus api package.
    """
    return about_get()


public_api_router_v0_1.include_router(test_request.public_router, prefix='/requests')
public_api_router_v0_1.include_router(compose.router, prefix='/composes')
