#!/bin/sh

terminate() {
    # optional terminate command, can be used to wait for specific conditions when the container should finish
    if [ -n "TERMINATE_COMMAND" ]; then
        $TERMINATE_COMMAND
    fi

    # no pid means we failed too early
    if [ -z "$PID" ]; then
        echo "Skipping termination, entrypoint failed too early"
        return
    fi

    echo "Terminating application"
    kill -TERM $PID

    # wait for the application to terminate
    wait $PID
}

# trap is needed to correctly propagate SIGTERM from container
trap terminate TERM INT

# helpers
error() { echo "Error: $@"; exit 1; }

# a name of the api to start
APP=$1

[ -z "$APP" ] && { error "No api to run passed to entrypoint script"; exit 1; }

case $APP in
    public)
        COMMAND="/opt/testing-farm/nucleus/api/bin/uvicorn --host 0.0.0.0 --port 8000 tft.nucleus.api.internal:api"
        ;;
    internal)
        COMMAND="/opt/testing-farm/nucleus/api/bin/uvicorn --host 0.0.0.0 --port 8000 tft.nucleus.api.public:api"
        ;;
    initdb)
        # Create database if doesn't exists
        if ! $(psql -h $DATABASE_HOSTNAME -p $DATABASE_PORT -U $DATABASE_USER -c 'USE nucleus;'); then
            psql -h $DATABASE_HOSTNAME -p $DATABASE_PORT -U $DATABASE_USER -c 'CREATE DATABASE nucleus;'
        fi
        # Initialize or upgrade the database to the latest version
        cd /opt/testing-farm/nucleus/api
        ./bin/alembic upgrade head || { echo "failed to upgrade DB Schema"; exit 1; }
        # Initialize records from server.yml
        exit 0
        ;;
    *)
        echo "Unknown app '$APP'"
        exit 1
        ;;
esac

# We run the command in background to get his PID which is used to properly
# terminate it with SIGTERM signal.

$COMMAND &
PID=$!

wait $PID
