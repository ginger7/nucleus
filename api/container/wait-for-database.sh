#!/bin/sh -x
#
# Script waits for database server availability
#

error() { echo "$@"; exit 1; }

[ -z "$DATABASE_HOSTNAME" ] && error "DATABASE_HOSTNAME environment variable not set"
[ -z "$DATABASE_PORT" ] && error "DATABASE_PORT environment variable not set"
[ -z "$DATABASE_USER" ] && error "DATABASE_USER environment variable not set"
[ -z "$WAIT_TIMEOUT" ] && WAIT_TIMEOUT=60
[ -z "$WAIT_TICK" ] && WAIT_TICK=1

check_cockroachdb() {
    timeout 1 psql -h $DATABASE_HOSTNAME -p $DATABASE_PORT -U $DATABASE_USER -c '\conninfo'
    return $?
}

time=0
until check_cockroachdb; do
    time=$((time + WAIT_TICK))
    [ $time -ge $WAIT_TIMEOUT ] && error "Failed to wait for database to start"
    sleep $WAIT_TICK
done
